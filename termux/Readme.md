## Termux config

This is my termux configuration.

Follow [this](https://itrendbuzz.com/install-and-configure-z-shell-on-termux/) to install `oh-my-zsh` on termux.
